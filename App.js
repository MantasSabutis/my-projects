import React from 'react';

import { StyleSheet, Text, View, Image, Button, Linking } from 'react-native';


import { StackNavigator } from 'react-navigation';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    paddingTop: 10,
    backgroundColor: '#ffffff',
  },
  image: {
    alignItems: 'center',
    width: 200,
    height: 200,
  },
  smallerImage: {
    alignItems: 'flex-start',
    width: 90,
    height: 90,
  },
  textName: {
    fontSize: 30,
    fontWeight: 'bold',
    textAlign: 'center',
  },
  textBold: {
    width: 250,
    height: 90,
    fontSize: 20,
    fontWeight: 'bold',
    textAlign: 'center',
  },
  textAboutMe: {
    fontSize: 15,
    color: '#BDBDBD',
  },
  textMyProfile: {
    fontSize: 15,
  },
});


class HomeScreen extends React.Component {
  render() {
    return (
      <View style={{ flex: 1, alignItems: 'center', justifyContent: 'space-around'}}>
        <Text style={styles.textName}>Mantas Sabutis</Text>
        <Image 
          style={styles.image}
          source={{uri: 'https://images-resrc.staticlp.com/C=AR1200x630/S=W1200,U/O=85/https://media.lonelyplanet.com/a/g/hi/t/4f3a115135f07760c4e0b99039e03674-best-beaches-in-nicaragua.jpg'}}
        />
        <Button color='#008000'
          title="My Profile"
          onPress={() => this.props.navigation.navigate('Profile')}
        />
      </View>
    );
  }
}

class ProfileScreen extends React.Component {
  render() {
    return (
      <View style={{flex: 1, flexDirection:'column', justifyContent: 'flex-start', alignItems: 'stretch'}}>
        <View style={{height: 50, backgroundColor: '#cccccc', flexDirection:'row', justifyContent: 'flex-start',alignItems: 'center'}}>
          <Text style={styles.textMyProfile}>My profile</Text>
        </View>
        <View style={{flexDirection:'row', justifyContent: 'flex-start', alignItems: 'center'}}>
          <Image
            style={styles.smallerImage}
            source={{uri: 'https://images-resrc.staticlp.com/C=AR1200x630/S=W1200,U/O=85/https://media.lonelyplanet.com/a/g/hi/t/4f3a115135f07760c4e0b99039e03674-best-beaches-in-nicaragua.jpg'}}
          />
          <Text style={styles.textName}>
            Mantas Sabutis
          </Text>
        </View>
        <View style={{height: 35, flexDirection:'row', justifyContent: 'flex-start',alignItems: 'center'}}>
          <Text style={styles.textAboutMe}>About me</Text>
        </View>
        <Text>I'm Mantas Sabutis from Vilnius, Lithuania. My email address is mantas.sabutis@gmail.com, phone number +37067519952</Text>
        <View style={{flexDirection:'row', justifyContent: 'center', alignItems: 'center'}}>
          <Text style={styles.textBold}>I live in City where current temperature is</Text>
        </View>
        <View style={{flexDirection:'row', justifyContent: 'center', alignItems: 'center'}}>
          <Button color='#008000'
            title="My Bitbucket profile"
            onPress={ ()=>{ Linking.openURL('https://bitbucket.org/MantasSabutis/')}}
          />
        </View>
      </View>
    );
  }
}

const RootStack = StackNavigator({
  Home: {
    screen: HomeScreen,
  },
  Profile: {
    screen: ProfileScreen,
  },
},
{
  initialRouteName: 'Home',
}
);

export default class App extends React.Component {
  render() {
    return <RootStack />;
  }

}


