package data;

public class Word {

    public static final String [] FIRST_SYMBOL_A_G = {"a", "b", "c", "d", "e", "f", "g"};
    public static final String [] FIRST_SYMBOL_H_N = {"h", "i", "j", "k", "l", "m", "n"};
    public static final String [] FIRST_SYMBOL_O_U = {"o", "p", "q", "d", "r", "s", "t", "u"};
    public static final String [] FIRST_SYMBOL_V_Z = {"v", "w", "x", "y", "z"};

    String word;
    int number;

    public Word(String word, int number) {
        this.word = word;
        this.number = number;
    }

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

}
