package service;

import data.Word;

import java.util.List;

public class Threads extends Thread {

    private Print print;
    private List<Word> list;

    public Threads(List<Word> list, Print print) {
        this.list = list;
        this.print = print;
    }

    @Override
    public void run() {
        try {
            print.print(list);
        } catch (Exception e) {
        }
    }

}
