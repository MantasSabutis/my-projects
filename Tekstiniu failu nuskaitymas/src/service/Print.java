package service;

import data.Word;

import java.util.List;

public class Print {

    public synchronized void print(List<Word> list) {
        System.out.println("*******************" + Thread.currentThread().getName() + "***********************");
        for (int i = 0; i < list.size(); i++) {
            System.out.println(Thread.currentThread().getName() + " Word - " + list.get(i).getWord() + " number - " + list.get(i).getNumber());
        }
    }

}
