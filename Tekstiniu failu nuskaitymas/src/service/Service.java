package service;

import data.Word;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Service {

    public List<String> allWordsList (String filesPath) throws FileNotFoundException {

        File folder = new File(filesPath);
        File[] listOfFiles = folder.listFiles();
        ArrayList<String> list = new ArrayList<>();
        for (File file : listOfFiles) {
            Scanner s = new Scanner(new File(String.valueOf(file)));
            while (s.hasNext()) {
                String words = s.next().toLowerCase().replaceAll("[^\\w\\s]", "");
                list.add(words);
            }
            s.close();
        }
        return list;
    }


    public List<Word> sortedWordList (List < String > list) {
        List<Word> result = new ArrayList<>();

        for (String word : list) {
            boolean wasWordAlreadyUsed = checkForDuplicateWords(result, word);
            if (!wasWordAlreadyUsed) {
                List<String> sameWordList = generateSameWordList(list, word);
                result.add(new Word(sameWordList.get(0), sameWordList.size()));
            } else if (wasWordAlreadyUsed) {
            }

        }
        return result;
    }


    public List<String> generateSameWordList (List < String > list, String word){
        List<String> sameWordList = new ArrayList<>();

        for (String wordFromAllWordList : list) {
            if (word.equals(wordFromAllWordList)) {
                sameWordList.add(wordFromAllWordList);
            }
        }
        return sameWordList;
    }


    public boolean checkForDuplicateWords (List < Word > list, String word){

        for (int i = 0; i < list.size(); i++) {
            if (word.equals(list.get(i).getWord())) {
                return true;
            }
        }
        return false;
    }


    public List<Word> generateWordListAccordingFirstLetter (List < Word > list, String []firstSymbol){
        List<Word> result = new ArrayList<>();

        for (int i = 0; i < firstSymbol.length; i++) {
            for (Word wordObject : list) {
                String firstLetter = wordObject.getWord().toLowerCase().substring(0, 1);
                if (firstSymbol[i].equals(firstLetter)) {
                    result.add(new Word(wordObject.getWord(), wordObject.getNumber()));
                }
            }
        }
        return result;
    }

}
