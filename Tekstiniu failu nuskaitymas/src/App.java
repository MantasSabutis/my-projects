import data.Word;
import service.Print;
import service.Service;
import service.Threads;

import java.io.IOException;
import java.util.List;

public class App {

    static Service service = new Service();
    static Print print = new Print();

    public static void main(String[] args) throws IOException {

        String filesPath = "......TYPE HERE FOLDER PATH WITH TEXT FILES......";

        List<String> allWordsList = service.allWordsList(filesPath);
        List<Word> sortedWordList = service.sortedWordList(allWordsList);

        List<Word> a_gWordList = service.generateWordListAccordingFirstLetter(sortedWordList, Word.FIRST_SYMBOL_A_G);
        List<Word> h_nWordList = service.generateWordListAccordingFirstLetter(sortedWordList, Word.FIRST_SYMBOL_H_N);
        List<Word> o_uWordList = service.generateWordListAccordingFirstLetter(sortedWordList, Word.FIRST_SYMBOL_O_U);
        List<Word> v_zWordList = service.generateWordListAccordingFirstLetter(sortedWordList, Word.FIRST_SYMBOL_V_Z);


        Thread t11 = new Thread(new Threads(a_gWordList, print));
        t11.setName("A-G");
        Thread t22 = new Thread(new Threads(h_nWordList, print));
        t22.setName("H-N");
        Thread t33 = new Thread(new Threads(o_uWordList, print));
        t33.setName("O-U");
        Thread t44 = new Thread(new Threads(v_zWordList, print));
        t44.setName("V-Z");

        t11.start();
        t22.start();
        t33.start();
        t44.start();
    }
}
